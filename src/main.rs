use std::{
    io::{stdin, stdout, Read, Write},
    sync::{mpsc::channel, Arc, Mutex},
    thread, time,
};

fn main() {
    with_loop();
}

/*
 * this will hold the mutex lock to right after the let statement in the loop, releasing the lock, and write thread 2 data first, then thread 1
 */
fn with_loop() -> () {
    let (tx, rx) = channel::<_>();
    let rx = Arc::new(Mutex::new(rx));

    let r1 = rx.clone();
    let r2 = rx.clone();

    tx.send(1);
    tx.send(2);

    let t1 = thread::spawn(move || loop {
        let n = r1.lock().unwrap().recv().unwrap();

        std::thread::sleep(time::Duration::from_millis(2000));
        println!("thread 1: {}", n);
    });

    let t2 = thread::spawn(move || {
        println!("waiting");
        let n = r2.lock().unwrap().recv().unwrap();

        println!("thread 2: {}", n);
    });

    t1.join();
    t2.join();
}

/*
 * this will write thread 2 data out first, then thread 1, because the mutex lock is released rigt after the let statement in both threads
 */
fn without_while() -> () {
    let (tx, rx) = channel::<_>();
    let rx = Arc::new(Mutex::new(rx));

    let r1 = rx.clone();
    let r2 = rx.clone();

    tx.send(1);
    tx.send(2);

    let t1 = thread::spawn(move || {
        let n = r1.lock().unwrap().recv().unwrap();

        std::thread::sleep(time::Duration::from_millis(2000));
        println!("thread 1: {}", n);
    });

    let t2 = thread::spawn(move || {
        println!("waiting");
        let n = r2.lock().unwrap().recv().unwrap();

        println!("thread 2: {}", n);
    });

    t1.join();
    t2.join();
}

/*
 * this will never write thread 2 data, because while will hold the lock and never release it
 */
fn with_while() -> () {
    let (tx, rx) = channel::<_>();
    let rx = Arc::new(Mutex::new(rx));

    let r1 = rx.clone();
    let r2 = rx.clone();

    tx.send(1);
    tx.send(2);

    let t1 = thread::spawn(move || {
        while let Ok(n) = r1.lock().unwrap().recv() {
            std::thread::sleep(time::Duration::from_millis(2000));
            println!("thread 1: {}", n);
        }
    });

    let t2 = thread::spawn(move || {
        println!("waiting");
        let n = r2.lock().unwrap().recv().unwrap();

        println!("thread 2: {}", n);
    });

    t1.join();
    t2.join();
}
